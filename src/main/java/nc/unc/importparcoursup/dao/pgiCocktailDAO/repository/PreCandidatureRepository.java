package nc.unc.importparcoursup.dao.pgiCocktailDAO.repository;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidature;
import org.springframework.data.repository.CrudRepository;

public interface PreCandidatureRepository extends CrudRepository<PreCandidature, Long> {
}
