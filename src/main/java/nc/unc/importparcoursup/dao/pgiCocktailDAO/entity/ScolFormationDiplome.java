package nc.unc.importparcoursup.dao.pgiCocktailDAO.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = ScolFormationDiplome.TABLE_NAME)
public class ScolFormationDiplome {
    public final static String TABLE_NAME = "SCOLARITE.SCOL_FORMATION_DIPLOME";
    @Id
    @Column(name = "FDIP_CODE")
    private String fdipcode;

    public String getFdipcode() {
        return fdipcode;
    }

    public void setFdipcode(String fdipcode) {
        this.fdipcode = fdipcode;
    }
}
