package nc.unc.importparcoursup.dao.pgiCocktailDAO.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = Bac.TABLE_NAME)
public class Bac {
    public final static String TABLE_NAME = "GRHUM.BAC";
    @Id
    @Column(name = "BAC_CODE")
    private String baccode;

    public Bac() {
    }

    public Bac(String baccode) {
        this.baccode = baccode;
    }

    public String getBaccode() {
        return baccode;
    }

    public void setBaccode(String baccode) {
        this.baccode = baccode;
    }
}
