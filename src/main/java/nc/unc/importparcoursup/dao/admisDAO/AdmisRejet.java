package nc.unc.importparcoursup.dao.admisDAO;

import nc.unc.importparcoursup.model.TypeRejet;

import javax.persistence.*;

@Entity
@Table(name = nc.unc.importparcoursup.dao.admisDAO.AdmisRejet.TABLE_NAME)
public class AdmisRejet {
    public final static String TABLE_NAME = "ADMIS_REJET";
    public final static String ADMIS_ID = "ADMIS_ID";
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;
    @ManyToOne
    @JoinColumn(name = ADMIS_ID)
    private Admis admis;
    @Enumerated(EnumType.STRING)
    private TypeRejet typeRejet;
    private String information;
    private boolean blocking;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Admis getAdmis() {
        return admis;
    }

    public void setAdmis(Admis admis) {
        this.admis = admis;
    }

    public TypeRejet getTypeRejet() {
        return typeRejet;
    }

    public void setTypeRejet(TypeRejet typeRejet) {
        this.typeRejet = typeRejet;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public boolean isBlocking() {
        return blocking;
    }

    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
    }
}
