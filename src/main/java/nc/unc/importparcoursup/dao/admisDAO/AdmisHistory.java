package nc.unc.importparcoursup.dao.admisDAO;

import nc.unc.importparcoursup.dao.IHistory;
import nc.unc.importparcoursup.model.UAI;
import nc.unc.importparcoursup.utils.MyFileUtils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = AdmisHistory.TABLE_NAME)
public class AdmisHistory implements IHistory {
    public final static String TABLE_NAME = "ADMIS_HISTORY";

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    private Date jobExecutionDate;

    private String fileCheckSum;

    @Enumerated(EnumType.STRING)
    private UAI uai;

    private Date extractionDate;

    private String createdBy;

    private String fileName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "admisHistory", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Admis> admis = new ArrayList<>();

    public AdmisHistory() {
        // Needed for hibernate
    }

    public AdmisHistory(String userLogin, String fileName, String fileCheckSum) {
        this.jobExecutionDate = new Date();
        this.extractionDate = MyFileUtils.getDateFromFileName(fileName);
        this.uai = MyFileUtils.getUAIFromFileName(fileName);
        this.fileCheckSum = fileCheckSum;
        this.createdBy = userLogin;
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getJobExecutionDate() {
        return jobExecutionDate;
    }

    public void setJobExecutionDate(Date jobExecutionDate) {
        this.jobExecutionDate = jobExecutionDate;
    }

    public List<Admis> getAdmis() {
        return admis;
    }

    public void setAdmis(List<Admis> admis) {
        this.admis = admis;
    }

    public String getFileCheckSum() {
        return fileCheckSum;
    }

    public void setFileCheckSum(String fileCheckSum) {
        this.fileCheckSum = fileCheckSum;
    }

    public UAI getUai() {
        return uai;
    }

    public void setUai(UAI uai) {
        this.uai = uai;
    }

    public Date getExtractionDate() {
        return extractionDate;
    }

    public void setExtractionDate(Date extractionDate) {
        this.extractionDate = extractionDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
