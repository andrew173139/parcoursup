package nc.unc.importparcoursup.view.informationrules;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import java.util.Optional;

public abstract class InformationElement<T> extends FlexLayout {
    private static final long serialVersionUID = 1L;
    protected Label valueLbl = new Label();

    public InformationElement() {
        super();
        VerticalLayout componentLeft = new VerticalLayout();
        VerticalLayout componentRight = new VerticalLayout();
        componentLeft.add(new Label(getLabel()));
        componentRight.add(valueLbl);
        add(componentLeft, componentRight);
    }

    public final void update(Optional<T> item) {
        item.ifPresentOrElse(i -> valueLbl.setText(updateValue(i)), () -> valueLbl.setText("-"));
    }

    protected abstract String updateValue(T item);

    protected abstract String getLabel();
}
