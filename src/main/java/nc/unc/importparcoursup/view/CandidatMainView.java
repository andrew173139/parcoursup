package nc.unc.importparcoursup.view;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import nc.unc.importparcoursup.Application;
import nc.unc.importparcoursup.dao.candidatDAO.repository.CandidatHistoryRepository;
import nc.unc.importparcoursup.dao.candidatDAO.repository.CandidatRepository;
import nc.unc.importparcoursup.view.components.CheckComponentCandidat;
import nc.unc.importparcoursup.view.components.ImportButtonComponent;
import nc.unc.importparcoursup.view.components.Stepper;
import nc.unc.importparcoursup.view.components.UploadComponent;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;

@Route(layout = TopBarLayout.class, value = "secured/candidatures")
@PageTitle(Application.APP_NAME)
public class CandidatMainView extends VerticalLayout {
    static final long serialVersionUID = 1L;

    private ImportButtonComponent goButton;
    private UploadComponent myUploadComponent;
    private CheckComponentCandidat checkComponent;
    private Stepper stepper;

    public CandidatMainView(@Autowired CandidatHistoryRepository candidatHistoryRepository,
                            @Autowired CandidatRepository candidatRepo, @Autowired @Qualifier("importCandidatJob") Job jobImportCsv, @Autowired JobLauncher jobLauncher,
                            @Value("${file.local-tmp-file}") String inputFile) {
        // init
        stepper = new Stepper("Selection du fichier", "Vérifications", "Importation des données");
        myUploadComponent = buildUploadComponent(inputFile);
        checkComponent = new CheckComponentCandidat(candidatHistoryRepository, myUploadComponent);
        goButton = new ImportButtonComponent(jobImportCsv, jobLauncher, myUploadComponent, stepper, checkComponent, null);

        // upload part
        add(stepper.getStepComponent(Stepper.Step.STEP_ONE));
        add(myUploadComponent);

        // verification
        add(stepper.getStepComponent(Stepper.Step.STEP_TWO));
        add(checkComponent);

        // Button part
        add(stepper.getStepComponent(Stepper.Step.STEP_THREE));
        add(goButton);

        // update data
        updateData();
    }

    private UploadComponent buildUploadComponent(String inputFile) {
        myUploadComponent = new UploadComponent(inputFile);
        myUploadComponent.addSucceededListener(event -> updateData());
        myUploadComponent.addFileRemoveListener(e -> updateData());
        return myUploadComponent;
    }

    private void updateData() {
        goButton.update();
        checkComponent.update();

        stepper.setStep(Stepper.Step.STEP_NONE);
        if (myUploadComponent.getFile().isPresent()) {
            stepper.setStep(Stepper.Step.STEP_ONE);
            if (checkComponent.isValid()) {
                stepper.setStep(Stepper.Step.STEP_TWO);
            }
        }
    }
}
