package nc.unc.importparcoursup.view.verificationrules;

import nc.unc.importparcoursup.utils.MyFileUtils;
import nc.unc.importparcoursup.view.components.UploadComponent;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Date;

public class FileDateBeforeToday extends CheckElement {
    private static final long serialVersionUID = 1L;

    public FileDateBeforeToday(UploadComponent upload) {
        super(upload);
    }

    @Override
    public boolean isValidCheck() {
        Date fileDate = MyFileUtils.getDateFromFileName(getFileName());
        return !fileDate.after(new Date());
    }

    @Override
    protected String getLabel() {
        return "Date <= à DDJ";
    }

}
