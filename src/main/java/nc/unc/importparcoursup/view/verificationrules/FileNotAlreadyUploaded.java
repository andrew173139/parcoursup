package nc.unc.importparcoursup.view.verificationrules;

import nc.unc.importparcoursup.dao.IHistory;
import nc.unc.importparcoursup.dao.IHistoryRepository;
import nc.unc.importparcoursup.utils.MyFileUtils;
import nc.unc.importparcoursup.view.components.UploadComponent;

public class FileNotAlreadyUploaded extends CheckElement {
    private static final long serialVersionUID = 1L;

    public FileNotAlreadyUploaded(UploadComponent upload, IHistoryRepository historyRepository) {
        super(upload, historyRepository);
    }

    @Override
    public boolean isValidCheck() {
        return isMD5New();
    }

    private boolean isMD5New() {
        String md5 = MyFileUtils.getMD5FromFile(uploadComponent.getFile().get());
        IHistory histo = historyRepository.findFirstByFileCheckSum(md5);
        return histo == null;
    }

    @Override
    protected String getLabel() {
        return "Fichier déjà traité";
    }
}
