package nc.unc.importparcoursup.view.components;

import com.opengamma.strata.collect.Unchecked;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import nc.unc.importparcoursup.utils.MyFileUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.security.core.context.SecurityContextHolder;

public class ImportButtonComponent extends Button {
    private static final long serialVersionUID = 1L;

    private static final String IMPORTER_LE_FICHIER = "Importer le fichier";

    private final Job job;
    private final JobLauncher jobLauncher;
    private final UploadComponent myUploadComponent;
    private final Stepper stepper;
    private final ICheckComponent checkComponent;
    private final InformationComponent infoComponent;

    public ImportButtonComponent(Job job, JobLauncher jobLauncher, UploadComponent myUploadComponent, Stepper stepper, ICheckComponent checkComponent, InformationComponent infoComponent) {
        super(IMPORTER_LE_FICHIER, new Icon(VaadinIcon.PLAY));
        this.job = job;
        this.jobLauncher = jobLauncher;
        this.myUploadComponent = myUploadComponent;
        this.stepper = stepper;
        this.addClickListener(event -> importFile());
        this.checkComponent = checkComponent;
        this.infoComponent = infoComponent;
    }

    private void launchJob() {
        Unchecked.wrap(() -> {
            JobParameters jobParams = new JobParametersBuilder()
                    .addLong("uniqueness", System.nanoTime())
                    .addString("fileName", myUploadComponent.getFile().get().getAbsolutePath())
                    .addString("userLogin", SecurityContextHolder.getContext().getAuthentication().getName())
                    .addString("uploadedFileName", myUploadComponent.getUploadedFileName())
                    .addString("fileCheckSum", MyFileUtils.getMD5FromFile(myUploadComponent.getFile().get()))
                    .toJobParameters();
            jobLauncher.run(job, jobParams);
        });
    }

    private void importFile() {
        UI ui = UI.getCurrent();
        ui.access(() -> {
            // disable button
            setEnabled(false);
            setText("En cours...");
            ui.push();

            // launch job
            myUploadComponent.getFile().ifPresent(file -> launchJob());

            // after the job execution
            setText(IMPORTER_LE_FICHIER);
            stepper.setStep(Stepper.Step.STEP_THREE);
            if (infoComponent != null) {
                infoComponent.update();
            }
            ui.push();
        });


    }

    public void update() {
        setEnabled(myUploadComponent.getFile().isPresent() && isValid());
    }

    private boolean isValid() {
        return checkComponent != null && checkComponent.isValid();
    }
}
