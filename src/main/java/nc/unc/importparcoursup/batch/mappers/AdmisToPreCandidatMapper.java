package nc.unc.importparcoursup.batch.mappers;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import nc.unc.importparcoursup.utils.MapperUtils;
import nc.unc.importparcoursup.utils.ScolUtils;
import org.apache.commons.lang3.StringUtils;

public class AdmisToPreCandidatMapper {

    public static void convertAdmisIntoPreCandidat(Admis scolarix, PreCandidat pre) {
        pre.setCAND_KEY(MapperUtils.stringToLong(scolarix.getCandNumero()));
        pre.setCAND_NUMERO(MapperUtils.stringToLong(scolarix.getCandNumero()));
        pre.setCAND_CIVILITE(scolarix.getCandCivilite());
        pre.setCAND_NOM(scolarix.getCandNom());
        pre.setCAND_PRENOM(scolarix.getCandPrenom());
        pre.setCAND_PRENOM2(scolarix.getCandPrenom2());
        pre.setCAND_DATE_NAIS(MapperUtils.stringToDate(scolarix.getCandDateNais()));
        pre.setCAND_TEL_PARENT(scolarix.getCandTelParent());
        pre.setCAND_BEA(scolarix.getCandBea());
        pre.setCAND_COMNAIS(scolarix.getCandComnais());
        pre.setCAND_EMAIL_SCOL(scolarix.getCandEmailScol());
        pre.setCAND_ANBAC(MapperUtils.stringToLong(scolarix.getCandAnbac()));
        pre.setCAND_BEA(scolarix.getCandBea());
        pre.setCAND_ADR1_PARENT(scolarix.getCandAdr1Parent());
        pre.setCAND_ADR2_PARENT(scolarix.getCandAdr2Parent());
        pre.setCAND_PORT_SCOL(scolarix.getCandPortScol());

        pre.setCAND_CP_PARENT(scolarix.getCandCpParent());
        pre.setCAND_VILLE_PARENT(scolarix.getCandVilleParent());
        pre.setCAND_VILLE_BAC(scolarix.getCandVilleBac());



        pre.setCAND_PAYS_PARENT(ScolUtils.getPays(scolarix.getCandPaysParent()));
        pre.setCAND_PAYS_SCOL(null);
        pre.setPAYS_CODE_DER_ETAB(ScolUtils.getPays(scolarix.getPaysCodeDerEtab()));
        pre.setPAYS_CODE_NAIS(ScolUtils.getPays(StringUtils.truncate(scolarix.getPaysCodeNais(), 3)));
        pre.setPAYS_ETAB_BAC(ScolUtils.getPays(scolarix.getPaysEtabBac()));

        pre.setHIST_ENS_DER_ETAB(StringUtils.truncate(scolarix.getHistEnsDerEtab(), 80));
        pre.setHIST_LIBELLE_DER_ETAB(scolarix.getHistLibelleDerEtab());
        pre.setHIST_VILLE_DER_ETAB(scolarix.getHistVilleDerEtab());

        pre.setDPTG_CODE_DER_ETAB(scolarix.getDptgCodeDerEtab());
        pre.setDPTG_CODE_NAIS(scolarix.getDptgCodeNais());
        pre.setDPTG_ETAB_BAC(scolarix.getDptgEtabBac());

        pre.setETAB_LIBELLE_BAC(scolarix.getEtabLibelleBac());
        pre.setETAB_CODE_BAC(scolarix.getEtabCodeBac());
        pre.setETAB_CODE_DER_ETAB(scolarix.getEtabCodeDerEtab());

        pre.setNAT_ORDRE(scolarix.getNatOrdre());
        pre.setACAD_ETAB_BAC(StringUtils.truncate(scolarix.getBacCode(), 3));
        pre.setBAC_CODE(scolarix.getBacCode());
    }

}
