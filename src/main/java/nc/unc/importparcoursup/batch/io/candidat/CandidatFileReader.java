package nc.unc.importparcoursup.batch.io.candidat;

import com.opencsv.bean.CsvToBeanBuilder;
import nc.unc.importparcoursup.dao.candidatDAO.Candidat;
import nc.unc.importparcoursup.dao.candidatDAO.CandidatHistory;
import org.springframework.batch.item.ItemReader;

import java.io.*;
import java.util.Iterator;

public class CandidatFileReader implements ItemReader<Candidat> {
    public static final char SEPARATOR = ';';
    private Iterator<Candidat> iteratorAdmis;
    private CandidatHistory candidatHistory;
    private Reader reader;

    public CandidatFileReader(File inputFile, CandidatHistory candidatHistory) throws IOException {
        this.candidatHistory = candidatHistory;
        reader = new BufferedReader(new FileReader(inputFile));
        iteratorAdmis = new CsvToBeanBuilder<Candidat>(reader).withType(Candidat.class)
                .withIgnoreLeadingWhiteSpace(true).withSeparator(SEPARATOR).build().iterator();
    }

    @Override
    public Candidat read() throws Exception {
        if (iteratorAdmis.hasNext()) {
            Candidat stu = iteratorAdmis.next();
            stu.setCandidatHistory(candidatHistory);
            return stu;
        }
        reader.close();
        return null;
    }
}
