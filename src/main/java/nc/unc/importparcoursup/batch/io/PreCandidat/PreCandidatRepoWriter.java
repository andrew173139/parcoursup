package nc.unc.importparcoursup.batch.io.PreCandidat;

import nc.unc.importparcoursup.dao.pgiCocktailDAO.entity.PreCandidat;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.PreCandidatRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class PreCandidatRepoWriter implements ItemWriter<PreCandidat> {
    private static final Logger log = LoggerFactory.getLogger(PreCandidatRepoWriter.class);
    private PreCandidatRepository repoPrecandidat;

    public PreCandidatRepoWriter(PreCandidatRepository repo) {
        this.repoPrecandidat = repo;
    }

    @Override
    public void write(List<? extends PreCandidat> items) {
        log.debug("Received the information of {} admis", items.size());
        items.forEach(preCandidat -> {
            log.debug("Saving precandidat - numéro: {} precandidature: {} ", preCandidat.getCAND_NUMERO(), preCandidat.getPreCandidature().getCANU_KEY());
            repoPrecandidat.save(preCandidat);
        });
    }
}