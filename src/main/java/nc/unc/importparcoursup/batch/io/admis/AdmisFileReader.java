package nc.unc.importparcoursup.batch.io.admis;

import com.opencsv.bean.CsvToBeanBuilder;
import nc.unc.importparcoursup.dao.admisDAO.Admis;
import nc.unc.importparcoursup.dao.admisDAO.AdmisHistory;
import org.springframework.batch.item.ItemReader;

import java.io.*;
import java.util.Iterator;

public class AdmisFileReader implements ItemReader<Admis> {
    public static final char SEPARATOR = '|';
    private Iterator<Admis> iteratorAdmis;
    private AdmisHistory admisHistory;
    private Reader reader;

    public AdmisFileReader(File inputFile, AdmisHistory admisHistory) throws IOException {
        this.admisHistory = admisHistory;
        reader = new BufferedReader(new FileReader(inputFile));
        iteratorAdmis = new CsvToBeanBuilder<Admis>(reader)
                .withType(Admis.class)
                .withIgnoreLeadingWhiteSpace(true)
                .withSeparator(SEPARATOR).build().iterator();
    }

    @Override
    public Admis read() throws Exception {
        if (iteratorAdmis.hasNext()) {
            Admis stu = iteratorAdmis.next();
            stu.setAdmisHistory(admisHistory);
            return stu;
        }
        reader.close();
        return null;
    }
}
