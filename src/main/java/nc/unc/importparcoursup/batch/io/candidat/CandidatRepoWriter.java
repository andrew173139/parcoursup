package nc.unc.importparcoursup.batch.io.candidat;

import nc.unc.importparcoursup.batch.io.PreCandidat.PreCandidatRepoWriter;
import nc.unc.importparcoursup.dao.candidatDAO.Candidat;
import nc.unc.importparcoursup.dao.candidatDAO.repository.CandidatRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class CandidatRepoWriter implements ItemWriter<Candidat> {
    private static final Logger log = LoggerFactory.getLogger(PreCandidatRepoWriter.class);

    private CandidatRepository candidatRepo;

    public CandidatRepoWriter(CandidatRepository cRepo) {
        this.candidatRepo = cRepo;
    }

    @Override
    public void write(List<? extends Candidat> items) {
        log.debug("Received the information of {} candidats", items.size());
        items.forEach(cand -> {
            log.info("Saving precandidat {} ", cand.getGroupe());
            candidatRepo.save(cand);
        });
    }
}