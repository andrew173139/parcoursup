package nc.unc.importparcoursup;

import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisPageableRepository;
import nc.unc.importparcoursup.dao.admisDAO.repository.AdmisRepository;
import nc.unc.importparcoursup.dao.pgiCocktailDAO.repository.PreCandidatRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The entry point of the Spring Boot application.
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class})
@EnableTransactionManagement
public class Application {
    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static final String APP_NAME = "ImportSup";

    @Autowired
    private AdmisRepository admisRepo;

    @Autowired
    private PreCandidatRepository preCandidatRepo;


    @Autowired
    AdmisPageableRepository admisPageableRepo;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {
        log.info("Test repo parcoursup (nb admiss): " + admisRepo.count());
        log.info("Test repo PGI Cocktail (nb pre-candidats):  " + preCandidatRepo.count());
        log.info("Nombre de lignes - last job (pageable): " + admisPageableRepo.count());
    }
}
