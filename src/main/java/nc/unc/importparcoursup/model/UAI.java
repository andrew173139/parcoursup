package nc.unc.importparcoursup.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum UAI {
    IUT("9830687E"),
    UNIV("9830445S");

    private String name;

    private static final Map<String, UAI> ENUM_MAP;

    UAI(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    static {
        Map<String, UAI> map = new ConcurrentHashMap<String, UAI>();
        Arrays.stream(UAI.values()).forEach(elem -> map.put(elem.getName(), elem));
        ENUM_MAP = Collections.unmodifiableMap(map);
    }

    public static UAI get(String name) {
        return ENUM_MAP.get(name);
    }
}