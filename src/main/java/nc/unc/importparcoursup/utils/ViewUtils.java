package nc.unc.importparcoursup.utils;

import com.opengamma.strata.collect.Unchecked;
import com.vaadin.flow.component.html.Label;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class ViewUtils {
    public static final String GREEN = "green";
    public static final String RED = "red";

    public static void checkedAndGreen(Label lbl) {
        checked(lbl);
        green(lbl);
    }

    public static void uncheckedAndRed(Label lbl) {
        unchecked(lbl);
        red(lbl);
    }

    public static void red(Label lbl) {
        lbl.removeClassName(GREEN);
        lbl.addClassName(RED);
    }

    public static void green(Label lbl) {
        lbl.removeClassName(RED);
        lbl.addClassName(GREEN);
    }

    public static void checked(Label lbl) {
        lbl.setText(SpecialCharUtils.CHECKED);
    }

    public static void unchecked(Label lbl) {
        lbl.setText(SpecialCharUtils.CROSSED);
    }

    public static Map<String, Object> beanProperties(Object bean) {
        return  Unchecked.wrap(() -> {
            return Arrays.asList(
                    Introspector.getBeanInfo(bean.getClass(), Object.class)
                            .getPropertyDescriptors()
            )
                    .stream()
                    // filter out properties with setters only
                    .filter(pd -> Objects.nonNull(pd.getReadMethod()))
                    .collect(Collectors.toMap(
                            // bean property name
                            PropertyDescriptor::getName,
                            pd -> { // invoke method to get value
                                try {
                                    return pd.getReadMethod().invoke(bean);
                                } catch (Exception e) {
                                    // replace this with better error handling
                                    return null;
                                }
                            }));
        });
    }
}
