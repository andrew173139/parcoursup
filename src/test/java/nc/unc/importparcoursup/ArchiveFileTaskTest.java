package nc.unc.importparcoursup;

import nc.unc.importparcoursup.batch.tasks.ArchiveFileTask;
import nc.unc.importparcoursup.helper.AdmisIUTFileTestHelper;
import nc.unc.importparcoursup.utils.MyFileUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.junit.Test;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

public class ArchiveFileTaskTest {
    @Test
    public void execute() throws Exception {
        // Lancement de la tache
        FileSystemResource outpuFileDir = new FileSystemResource(AdmisIUTFileTestHelper.TMP_DIR + "/tmpOut/");
        File file = new AdmisIUTFileTestHelper().createOneLineFile().getFile();
        new ArchiveFileTask(file, outpuFileDir).execute(null, null);

        // controles
        Collection<File> resultCol = FileUtils.listFiles(outpuFileDir.getFile(), TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
        assertThat(resultCol.size()).isEqualTo(1);

        File resultFile = resultCol.stream().findFirst().get();
        assertThat(MyFileUtils.readFile(resultFile)).isEqualTo(AdmisIUTFileTestHelper.LINE_CONTENT);
    }
}
