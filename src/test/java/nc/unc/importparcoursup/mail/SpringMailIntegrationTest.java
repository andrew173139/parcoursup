package nc.unc.importparcoursup.mail;

import com.icegreen.greenmail.junit.GreenMailRule;
import com.icegreen.greenmail.util.ServerSetupTest;
import nc.unc.importparcoursup.batch.mail.EmailService;
import nc.unc.importparcoursup.batch.mail.Mail;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SpringMailIntegrationTest {
    @Autowired
    private EmailService emailService;

    @Rule
    public final GreenMailRule greenMail = new GreenMailRule(ServerSetupTest.SMTP);

    @Test
    public void testSend() throws MessagingException, IOException {
        greenMail.start();
        greenMail.setUser("test@test.com", "test@test.com", "pwd");
        Mail mail = new Mail();
        mail.setFrom("no-reply@memorynotfound.com");
        mail.setTo("info@memorynotfound.com");
        mail.setSubject("Spring Mail Integration Testing with JUnit and GreenMail Example");
        mail.setContent("We show how to write Integration Tests using Spring and GreenMail.");
        emailService.sendSimpleMessage(mail);
        MimeMessage[] receivedMessages = greenMail.getReceivedMessages();
        assertThat(receivedMessages.length).isEqualTo(1);
        MimeMessage current = receivedMessages[0];
        assertThat(mail.getSubject()).isEqualTo(current.getSubject());
        assertThat(mail.getTo()).isEqualTo(current.getAllRecipients()[0].toString());
        assertThat(String.valueOf(current.getContent())).contains(mail.getContent());
        greenMail.stop();
    }
}
