package nc.unc.importparcoursup.io;

import nc.unc.importparcoursup.batch.io.candidat.CandidatFileReader;
import nc.unc.importparcoursup.dao.candidatDAO.Candidat;
import nc.unc.importparcoursup.helper.CandidatFileHelper;
import nc.unc.importparcoursup.helper.ModelTestHelper;
import nc.unc.importparcoursup.utils.MyFileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

//@RunWith(SpringRunner.class)
//@DataJpaTest
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CandidatFileReaderTest {

    @Test
    public void testRead() throws Exception {
        Long histoId = 1234L;
        ModelTestHelper modelHelper = new ModelTestHelper();
        File candidatFile = new CandidatFileHelper().createOneLineFile().addLineToFile(CandidatFileHelper.LINE_1).addLineToFile(CandidatFileHelper.LINE_2).getFile();

        System.out.println("#########################################################################################\n"+MyFileUtils.readFile(candidatFile));

        // Création du reader
        CandidatFileReader reader = new CandidatFileReader(candidatFile, modelHelper.createCandidatHistory(histoId, modelHelper.today()));

        Candidat resultCandidat = reader.read();
        assertThat(resultCandidat.getGroupe()).isEqualTo("104");
        resultCandidat = reader.read();
        assertThat(resultCandidat.getGroupe()).isEqualTo("103");
        resultCandidat = reader.read();
        assertThat(resultCandidat).isNull();
    }
}
