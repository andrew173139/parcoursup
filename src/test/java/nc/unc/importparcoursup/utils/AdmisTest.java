package nc.unc.importparcoursup.utils;

import nc.unc.importparcoursup.dao.admisDAO.Admis;
import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AdmisTest {
    @Test
    public void setCandFormationChoixTest() {
        Admis admis = new Admis();
        admis.setCandFormationChoix("");
        assertThat(admis.getFDipCode()).isEqualTo("");
        assertThat(admis.getFSpnKey()).isEqualTo("");
        admis.setCandFormationChoix("123;456");
        assertThat(admis.getFDipCode()).isEqualTo("123");
        assertThat(admis.getFSpnKey()).isEqualTo("456");
    }
}
