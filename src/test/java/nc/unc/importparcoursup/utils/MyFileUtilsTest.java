package nc.unc.importparcoursup.utils;

import nc.unc.importparcoursup.helper.AdmisIUTFileTestHelper;
import nc.unc.importparcoursup.model.UAI;
import org.junit.Test;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class MyFileUtilsTest {

    @Test
    public void testIsEmpty() {
        File emptyFile = new AdmisIUTFileTestHelper().createEmptyFile().getFile();
        assertThat(MyFileUtils.isEmpty(emptyFile)).isTrue();

    }

    @Test
    public void testIsNotEmpty() {
        File notEmptyFile = new AdmisIUTFileTestHelper().createOneLineFile().getFile();
        assertThat(MyFileUtils.isEmpty(notEmptyFile)).isFalse();
    }

    @Test
    public void getUAITest() {
        assertThat(MyFileUtils.getUAIFromFileName("20181201_2352_" + UAI.IUT.getName() + ".csv")).isEqualTo(UAI.IUT);
        assertThat(MyFileUtils.getUAIFromFileName("20181212_1212_" + UAI.UNIV.getName() + ".csv")).isEqualTo(UAI.UNIV);
        assertThatThrownBy(() -> MyFileUtils.getUAIFromFileName("20181201_2352_010145A.csv"))
                .isInstanceOf(Exception.class).hasMessageContaining(MyFileUtils.FILE_FORMAT_ERR);
    }

    @Test
    public void getDateTest() throws ParseException {
        // ok
        Date expected = new SimpleDateFormat(MyFileUtils.FILE_DATE_FORMAT).parse("20181201_2352");
        assertThat(MyFileUtils.getDateFromFileName("20181201_2352_" + UAI.IUT.getName() + ".csv")).isEqualTo(expected);

        // wrong date
        assertThatThrownBy(() -> MyFileUtils.getDateFromFileName("20181301_2352_" + UAI.IUT.getName() + ".csv"))
                .isInstanceOf(Exception.class).hasMessageContaining("Unparseable date: ");
    }

    @Test
    public void validateFileNameTest() {
        // wrong aui (not in enum list)
        String WRONG_ENUM = "0170145T";
        assertThatThrownBy(() -> MyFileUtils.checkFileName("20181201_2352_" + WRONG_ENUM + ".csv"))
                .isInstanceOf(Exception.class).hasMessageContaining(MyFileUtils.FILE_FORMAT_ERR);
    }
}
