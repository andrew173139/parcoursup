Feature: Intégration des nouveaux précandidats via le job

  Scenario: Intégration d'un précandidat
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 11        |
    Given Le fichier des admissions IUT
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        |    11    | john | S       | 100      |
    When Lancer le job integration parcoursup
    Then Il y a 1 precandidat(s) dans la base precandidat
    Then Precandidat 100 créé avec code diplome 11

  Scenario: Intégration de 2 nouveaux précandidats
    Given Les codes pays
      | PAYS_CODE |
      | 100       |
    Given Les codes bac
      | BAC_CODE |
      | S        |
    Given Les codes diplomes
      | FSPN_KEY | FDIP_CODE |
      | 1        | 11        |
      | 2        | 22        |
    Given Le fichier des admissions UNIV
      | candnumero | fdipcode | nom  | baccode | PaysCodeNais |
      | 100        | 11       | john | S       | 100      |
      | 200        | 22       | jack | S       | 100      |
    When Lancer le job integration parcoursup
    Then Il y a 2 precandidat(s) dans la base precandidat
    Then Precandidat 100 créé avec code diplome 11
    Then Precandidat 200 créé avec code diplome 22